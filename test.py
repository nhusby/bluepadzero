from gpiozero import Motor, OutputDevice
from time import sleep

motor1 = Motor(24, 27)
motor1_enable = OutputDevice(5, initial_value=1)
motor2 = Motor(6, 22)
motor2_enable = OutputDevice(17, initial_value=1)
motor3 = Motor(23, 16)
motor3_enable = OutputDevice(12, initial_value=1)
motor4 = Motor(13, 18)
motor4_enable = OutputDevice(25, initial_value=1)

motor1.forward()
motor1.forward()
motor3.backward()
motor4.backward()

sleep(1)

motor1.reverse()
motor2.reverse()
motor3.reverse()
motor4.reverse()

sleep(1)

motor1.forward()
motor1.forward()

sleep(1)

motor1.reverse()
motor2.reverse()
motor3.reverse()
motor4.reverse()

sleep(1)

motor1.stop()
motor2.stop()
motor3.stop()
motor4.stop()